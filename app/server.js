// load environmental variables contained in .env file
require("dotenv").config();

// const fs = require('fs')
const express = require("express");
const app = express();
// const browserify = require('browserify-middleware')
const path = require("path");
const http = require("http");
const configureSSL = require("./configure-ssl.js");

// var server = configureSSL(app)
// Create HTTP server instead HTTPS
var server = http.createServer(app);
let log = require("./util/get_logger.js")({
  label: "HydraServer",
});

// TURN server access
// var twilio = require('twilio')
// require('dotenv').config()

// if (process.env.TWILIO_SID) {
//   var twilio_client = new twilio(process.env.TWILIO_SID, process.env.TWILIO_AUTH)
// }

var io = require("socket.io")(server);
// require('./twitter-gallery.js')(app)

// crear un servidor en puerto 8000
var serverPort = process.env.PORT || 8000;
server.listen(serverPort, function () {
  log.info("Server available at http://localhost:" + serverPort);
});

// look up uuid by entiring socket id
var userFromSocket = {};

// lookup socket id by entering uuid
var socketFromUser = {};

// new connection to websocket server
io.on("connection", function (socket) {
  log.debug(`new connection ${socket.id}`);
  var thisRoom = null;
  socket.on("join", function (room, _userData) {
    thisRoom = room;
    log.info(`New user data: ${JSON.stringify(_userData)}`);
    if (_userData.uuid) {
      userFromSocket[socket.id] = _userData.uuid;
      socketFromUser[_userData.uuid] = socket.id;
    } else {
      log.error("no user data!");
    }
    // Get the list of peers in the room
    var peers = io.nsps["/"].adapter.rooms[room]
      ? Object.keys(io.nsps["/"].adapter.rooms[room].sockets)
      : [];

    io.of("/")
      .in(room)
      .clients(function (error, clients) {
        if (error) throw error;
        log.debug(`Room(${room.id}) clients: ${clients}`); // => [Anw2LatarvGVVXEIAAAD]
      });

    // And then add the client to the room
    socket.join(room);

    // send updated list of peers to all clients in room
    // io.sockets.emit('peers', peerUuids);
    socket.to(thisRoom).emit(`New user(${_userData.uuid}) in room`);
  });

  socket.on("broadcast", function (data) {
    log.debug("broadcasting", data, socket.room);
    socket.to(thisRoom).emit("broadcast", data);
  });

  // pass message from one peer to another
  socket.on("message", function (data) {
    var client = io.sockets.connected[socketFromUser[data.id]];
    client &&
      client.emit("message", {
        id: userFromSocket[socket.id],
        label: socket.label,
        message: data.message,
        type: data.type,
      });
  });

  socket.on("signal", function (data) {
    console.log("forwarding signal " + JSON.stringify(data));
    var client = io.sockets.connected[socketFromUser[data.id]];
    client &&
      client.emit("signal", {
        id: userFromSocket[socket.id],
        label: socket.label,
        signal: data.signal,
      });
  });
  // TO DO: on disconnect, remove from label dictionary
});

app.use(express.static(path.join(path.resolve("./"), "/public")));
