FROM node:lts-alpine as base
WORKDIR /usr/src/app/
COPY package.json package-lock.json /usr/src/app/
RUN npm install --production
EXPOSE 8000
COPY . .

FROM base as production
ENV NODE_ENV=production
CMD npm run start

FROM base as develop
ENV NODE_ENV=development
RUN npm install --only=dev
CMD npm run dev
