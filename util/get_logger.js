const { createLogger, format, transports } = require('winston');

const myFormat = format.printf(({ level, message, label }) => {
  return `[${label}] ${level}: ${message}`;
});

module.exports = ({label, service}) => {
  return createLogger({
    defaultMeta: { service: service },
    transports: [
      new transports.Console({
        format: format.combine(
          format.colorize(),
          format.label({ label: label }),
          format.errors({ stack: true }),
          myFormat
        )
      })
    ]
  })
}